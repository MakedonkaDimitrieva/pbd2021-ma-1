package si.uni_lj.fri.pbd.miniapp1

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_contacts.view.*


class ContactsFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_contacts, container, false)
        var button_new_contact = view.fab as FloatingActionButton
        button_new_contact.setOnClickListener{
            requireActivity().run{
                startActivity(Intent(this, AddContactActivity::class.java))
                //finish()
            }
        }
        return view
    }
}