package si.uni_lj.fri.pbd.miniapp1


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.fragment_contacts.*
import android.widget.ArrayAdapter

class MainActivity : AppCompatActivity() {

    companion object {
        const val RECORD_REQUEST_CODE_CONTACTS = 1
        const val TAG = "MainActivity"
    }


    var drawerLayout: DrawerLayout? = null
    var actionBarToggle: ActionBarDrawerToggle? = null

    var navView: NavigationView? = null



    var listView: ListView? = null
    var StoreContacts: ArrayList<String>? = null
    lateinit var arrayAdapter: ArrayAdapter<String>
    lateinit var cursor: Cursor
    lateinit var name: String
    lateinit var phoneNumber: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // Call findViewById on the DrawerLayout
        drawerLayout = findViewById(R.id.activity_main)

        // Pass  the ActionBarToggle action into the drawerListener
        actionBarToggle = ActionBarDrawerToggle(this, drawerLayout, 0, 0)
        drawerLayout?.addDrawerListener(actionBarToggle!!)

        // Display the icon to launch the drawer
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Call syncState() on the action bar so it'll automatically change to the back button when the drawer layout is open
        actionBarToggle?.syncState()


        // Call findViewById on the NavigationView
        navView = findViewById(R.id.navView)




        // Call setNavigationItemSelectedListener on the NavigationView to detect when items are clicked
        navView?.setNavigationItemSelectedListener { menuItem ->

            var fragment:Fragment?
            var fragmentClass:Class<*>

            when (menuItem.itemId) {
                R.id.home -> {
                    fragmentClass = HomeFragment::class.java
                    fragment = fragmentClass.newInstance() as HomeFragment
                    Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
                }
                R.id.contacts -> {
                    fragmentClass = ContactsFragment::class.java
                    fragment = fragmentClass.newInstance() as ContactsFragment
                    Toast.makeText(this, "Contacts", Toast.LENGTH_SHORT).show()
                    setUpPermissionContacts()
                }
                R.id.message -> {
                    fragmentClass = MessageFragment::class.java
                    fragment = fragmentClass.newInstance() as Fragment
                    Toast.makeText(this, "Message", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    fragmentClass = HomeFragment::class.java
                    fragment = fragmentClass.newInstance() as HomeFragment
                    Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
                }
            }

            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit()
            menuItem.isChecked = true
            title = menuItem.title

            drawerLayout?.closeDrawers()

            return@setNavigationItemSelectedListener true
        }
    }


    /*override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(actionBarToggle?.onOptionsItemSelected(item)!!)
            return true
        return super.onOptionsItemSelected(item)
    }*/


    // override the onSupportNavigateUp() function to launch the Drawer when the icon is clicked
    override fun onSupportNavigateUp(): Boolean {
        drawerLayout!!.openDrawer(navView!!)
        return true
    }

    // override the onBackPressed() function to close the Drawer when the back button is clicked
    override fun onBackPressed() {
        if(this.drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun setUpPermissionContacts() {
        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.READ_CONTACTS)
        if(permission != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission to read contacts denied")
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)) {
                val builder = AlertDialog.Builder(this)
                with(builder) {
                    setMessage("Permission to access contacts required")
                    setTitle("Permission required")
                    setPositiveButton("OK") {p0, p1 ->
                        Log.d(TAG, "Clicked")
                        makeRequestContacts()
                    }
                }
                val dialog = builder.create()
                dialog.show()
            }
            else {
                makeRequestContacts()
            }
        }
    }

    private fun makeRequestContacts() {
        ActivityCompat.requestPermissions(this,
        arrayOf(Manifest.permission.READ_CONTACTS),
        RECORD_REQUEST_CODE_CONTACTS)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == RECORD_REQUEST_CODE_CONTACTS) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permission has been denied by the user")
                var contacts = ContactsFragment()
                contacts = getContactsIntoArrayList()
                supportFragmentManager.beginTransaction().replace(android.R.id.content, contacts).commit()
            }
            else {
                Log.d(TAG, "Permission has been granted by the user")
            }
        }
    }

    private fun getContactsIntoArrayList(): ContactsFragment {
        cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null)!!
        while(cursor.moveToNext()) {
            name = cursor.getColumnName(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
            StoreContacts?.add("$name $phoneNumber")
        }
        listView = findViewById(R.id.list_contacts)
        arrayAdapter = ArrayAdapter<String>(this, R.layout.contacts_list_item, R.id.text1, StoreContacts!!)
        listView!!.adapter = arrayAdapter
        cursor.close()
        return listView as ContactsFragment
    }
}