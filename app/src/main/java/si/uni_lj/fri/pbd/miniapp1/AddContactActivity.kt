package si.uni_lj.fri.pbd.miniapp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.ContactsContract.Contacts.CONTENT_URI
import android.provider.ContactsContract.Data.CONTENT_URI
import android.provider.ContactsContract.Directory.CONTENT_URI
import android.view.View
import android.widget.Button
import androidx.fragment.app.FragmentTransaction

class AddContactActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_contact)

        var button_add_contact = findViewById<Button>(R.id.add_contact)

    }

    override fun onClick(v: View?) {
        val Intent = Intent(Intent.ACTION_DEFAULT, ContactsContract.Contacts.CONTENT_URI)
        startActivity(intent)
    }

}