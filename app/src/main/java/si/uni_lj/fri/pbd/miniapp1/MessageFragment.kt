package si.uni_lj.fri.pbd.miniapp1

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_message.view.*

class MessageFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view =  inflater.inflate(R.layout.fragment_message, container, false)

        var button_email = view.button_email as Button

        button_email.setOnClickListener{
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, "")
            emailIntent.putExtra(Intent.EXTRA_CC, "")
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "PBD2021 Group Email")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Sent from my Android mini app 1")
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."))
                //finish()
            }
            catch (ex:android.content.ActivityNotFoundException) {
                Toast.makeText(activity, "There is no email client installed", Toast.LENGTH_SHORT).show()
            }
        }

        var button_mms = view.button_mms as Button

        button_mms.setOnClickListener{
            val mmsIntent = Intent(Intent.ACTION_SEND)
            mmsIntent.data = Uri.parse("mms:")
            mmsIntent.type = "text/plain"
            mmsIntent.putExtra(Intent.EXTRA_PHONE_NUMBER, "");
            mmsIntent.putExtra(Intent.EXTRA_TEXT, "Sent from my Android mini app 1")
            try {
                startActivity(Intent.createChooser(mmsIntent, "Send mms..."))
                //finish()
            }
            catch (ex:android.content.ActivityNotFoundException) {
                Toast.makeText(activity, "MMS failed, please try again", Toast.LENGTH_SHORT).show()
            }
        }

        return view
    }
}